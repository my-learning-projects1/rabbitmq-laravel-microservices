<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    /**
     * @return Collection
     */
    public function index(): Collection
    {
        return Product::all();
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function show(int $id): mixed
    {
        return Product::find($id);
    }

    /**
     * @param  Request  $request
     * @return Application|ResponseFactory|\Illuminate\Http\Response
     */
    public function store(Request $request): \Illuminate\Http\Response|Application|ResponseFactory
    {
        $product = Product::create(
            $request->only('title', 'image')
        );

        return response($product, Response::HTTP_CREATED);
    }

    /**
     * @param  int  $id
     * @param  Request  $request
     * @return Application|ResponseFactory|\Illuminate\Http\Response
     */
    public function update(int $id, Request $request): \Illuminate\Http\Response|Application|ResponseFactory
    {
        $product = Product::find($id);
        $product->update(
            $request->only('title', 'image')
        );

        return response($product, Response::HTTP_ACCEPTED);
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response|Application|ResponseFactory
     */
    public function destroy(int $id): \Illuminate\Http\Response|Application|ResponseFactory
    {
        Product::destroy($id);
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
